This directory contains the contracts for the bridge between Dune and Free TON:

* DuneGiver.sol : this is the master contract containing all the TON coins.
   It receives requests from user swap contracts, verify that they are
   ok, and send back the TON coins.

* DuneRootSwap.sol : this contract has 2 tasks:
  * it manages  the list of official relays (oracles) allowed to sign
    transactions that replicates orders on Dune on the Free TON side
  * it deploys user swap contracts on relays' demands

* DuneUserSwap.sol : this is the user swap contract that is created by
  the DuneRootSwap contract on a relay's demand. Every swap request
  for the same user pubkey is handled by this contract, confirmed by a
  minimal number of relays. When the request is confirmed, a request is
  sent to the DuneGiver to receive the tokens. When the user reveals the
  secret, the tokens are then transfered to his account.

* DuneEvents.sol: this contract receives messages every time an important
  action happened in the merge. Useful to watch by the relay.

* FakeDePool.sol: a faked DePool contract, that can be used to test vested
  accounts locally

To build:

```
make all
```

To import contract in ft without building them (if you don't have solc):

```
make import
```


