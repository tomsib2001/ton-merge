/* Interface IDuneGiver */

pragma ton-solidity >= 0.37.0;

interface IDuneGiver {

  function creditOrder(
                         string credit_id,
                         uint256 swap_hash,
                         uint128 ton_amount,
                         uint256 pubkey
                         ) external ;
  
  function setSwapInfo(
                       TvmCell walletCode,
                       uint64 expiration_date,
                       uint64 swap_expiration_time
                       ) external ;
}
