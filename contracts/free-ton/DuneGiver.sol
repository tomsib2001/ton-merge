/*
  Implementation of contract DuneGiver
 */

pragma ton-solidity >= 0.37.0;

pragma AbiHeader expire;
pragma AbiHeader pubkey;

import "./IDuneGiver.sol";
import "./IDuneRootSwap.sol";
import "./IDuneEvents.sol";
import "./DuneSwapBase.sol";
import "./DuneHashBase.sol";

contract DuneGiver is
  IDuneGiver,
  DuneSwapBase, DuneConstBase, DuneHashBase {
  
  // address of the DuneEvents contract
  address public g_event_address ;
  // address of the main FreeTON giver
  address public g_freeton_giver ;
  uint64 public g_swap_expiration_time ;

  event OrderCredited ( string order_id, uint256 pubkey, uint128 ton_amount );



  modifier OwnerSet() {
    require( tvm.pubkey() != 0, EXN_MISSING_PUBKEY );
    _;
  }

  modifier RootSet() {
    require( g_root_address != address(0), EXN_NOT_INITIALIZED );
    _;
  }


  // @param freeton_giver The official FreeTON giver, so that we can send
  //     back unused tokens after the expiration date
  constructor( address freeton_giver ) public OwnerSet() AuthOwner() {
    tvm.accept();

    g_freeton_giver = freeton_giver ;
  }



  /* Initialize the giver with the address of the DuneRootSwap contract
     used to deploy DuneUserSwap contracts. Also, the expiration date
     can be used to prevent any transfer after a given time.
  */
  function init( address root_address,
                 address event_address,
                 uint64  root_value) public
    AuthOwner()
  {
    require( g_root_address == address(0) , EXN_INIT_TWICE );
    require( address(this).balance > root_value + ( 1000 ton ) ,
             EXN_BALANCE_TOO_LOW );
    tvm.accept();

    
    g_root_address = root_address ;
    g_giver_address = address(this) ;
    g_event_address = event_address ;

    IDuneRootSwap( root_address ).getSwapInfo
      { value: root_value }( event_address );
  }



  // @dev Set the userSwapCode and expiration date sent by the
  //      DuneRootSwap contract as a reply to getSwapInfo()
  // @param userSwapCode The code of the DuneUserSwap to be able to verify
  //        addresses
  // @param merge_expiration_date The date at which no more transfers are allowed
  function setSwapInfo(TvmCell duneUserSwapCode,
                       uint64 merge_expiration_date,
                       uint64 swap_expiration_time)
    public override RootSet() AuthRoot()
  {
    g_merge_expiration_date = merge_expiration_date ;
    g_swap_expiration_time = swap_expiration_time ;
    g_duneUserSwapCode = duneUserSwapCode ;
    IDuneEvents( g_event_address ).init
      { flag: 1 }( g_root_address, duneUserSwapCode, merge_expiration_date);
  }



  // @dev Transfer TON tokens to a DuneUserSwap contract
  // @param credit_id Unique identifier of the swap
  // @param ton_amount Amount of nanoton to transfer back
  // @param pubkey Public key associated with the swap user
  function creditOrder(
                         string order_id,
                         uint256 swap_hash,
                         uint128 ton_amount,
                         uint256 pubkey) public override RootSet()
  {
    require( uint64(now) < g_merge_expiration_date, EXN_SWAP_EXPIRED );
    tvm.accept();

    bool credited = false ;
    
    address computed_addr = _getUserSwapAddress( pubkey, swap_hash );

    // Fail if the caller is not the DuneUserSwap associated with this
    // pubkey.
    require( msg.sender == computed_addr, EXN_AUTH_FAILED );

    if( address(this).balance >= ton_amount ){

      // Otherwise, send back the requested tokens
      // TODO: it may fail if we don't have enough balance
      IDuneUserSwap( computed_addr ).receiveCredit
        { value: ton_amount + ( 0.1 ton ), bounce: true, flag: 0 }
      ();

      emit OrderCredited ( order_id, pubkey, ton_amount );
      credited = true ;
    } else {
      IDuneUserSwap( msg.sender ).creditDenied();
    }
    IDuneEvents( g_event_address ).orderCredited ( order_id, credited );
  }




  // @dev Send back unused tokens to the main giver when expiration
  // date is passed.
  // @caller admin with same pubkey. Since this contract MUST exist until
  // all other contracts have been closed, only the admin should be able
  // close it.
  function closeSwap() public AuthOwner() {
    tvm.accept();

    require( uint64(now) > g_merge_expiration_date , EXN_SWAP_NOT_EXPIRED ) ;

    selfdestruct( g_freeton_giver );
  }




  fallback () external {}

  function get() public view returns(
                                address root_address,
                                address giver_address,
                                address event_address,
                                uint64 merge_expiration_date,
                                uint64 swap_expiration_time,
                                address freeton_giver
  ) {
    root_address = g_root_address ;
    giver_address = g_giver_address ;
    event_address = g_event_address ;
    merge_expiration_date = g_merge_expiration_date;
    swap_expiration_time = g_swap_expiration_time ;
    freeton_giver = g_freeton_giver ;
  }

}


