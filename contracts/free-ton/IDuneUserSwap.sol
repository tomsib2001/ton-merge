/* Interface of a User Swap Contract */

pragma ton-solidity >= 0.37.0;

interface IDuneUserSwap {

  function receiveCredit() external ;
  function creditDenied() external ;
  
  function updateRelays( uint256[] relays,
                         uint8[] indexes,
                         uint8 nreqs) external;
}
