pragma ton-solidity >= 0.37.0;

pragma AbiHeader expire;
pragma AbiHeader pubkey;

import "./IDuneUserSwap.sol";
import "./IDuneGiver.sol";
import "./IDePool.sol";
import "./IParticipant.sol";
import "./IDuneEvents.sol";
import "./IDuneRootSwap.sol";

import "./DuneRelayBase.sol";
import "./DuneConstBase.sol";
import "./DuneHashBase.sol";

/* We may create a specific contract DuneEventContract to be able
   to monitor everything while observing only one contract */

contract DuneUserSwap is IDuneUserSwap, IParticipant,
  DuneRelayBase, DuneConstBase, DuneHashBase
{
  uint8 constant EXN_AUTH_FAILED = 100 ;

  event OrderStateChanged ( uint8 state );
  event OrderRevealed ( string secret );

  address static s_giver_address ;  // DuneGiver
  address static s_root_address ;   // DuneRootSwap
  uint256 static s_swap_hash ;
  address public g_event_address ;
  uint64 public g_merge_expiration_date ; // nothing can happen after this
  uint64 public g_swap_expiration_time ; // nothing can happen after this

  bool g_initialized = false ;
  
  string g_order_id ;
  uint256 g_hashed_secret ;
  uint64 g_ton_amount ;
  address g_dest ;
  address g_depool ; // can be address(0)
  uint8 g_state ;
  uint32 g_state_count ;
  uint8 g_nconfirms ;
  uint64 g_voteMask ;
  uint64 g_swap_expiration_date ;
  uint8 g_failed_revelations ;

  uint256 g_deployer_pubkey ;



  modifier AuthOwner() {
    require( msg.pubkey() == tvm.pubkey(), EXN_AUTH_FAILED );
    _;
  }

  modifier AuthOwnerOrRelay() {
    if ( msg.pubkey() != tvm.pubkey() ) _isRelay( msg.pubkey() );
    _;
  }

  modifier AuthRoot() {
    require( msg.sender == s_root_address, EXN_AUTH_FAILED );
    _;
  }

  modifier AuthGiver() {
    require( msg.sender == s_giver_address, EXN_AUTH_FAILED );
    _;
  }




  constructor(uint64 merge_expiration_date,
              uint8 nreqs,
              uint256[] relays,
              uint8[] indexes,
              address event_address,
              uint64 swap_expiration_time,
              uint256 deployer_pubkey) public AuthRoot()
    {
      tvm.accept();

      // We don't check nreqs and relays, assuming they are correct by
      // construction. Anyway, if false, we are fucked.
      g_nreqs = nreqs ;
      uint256 len = relays.length;
      for ( uint8 i = 0 ; i < len ; i++ ){
        g_relays[ relays[i] ] = indexes[i] ;
      }
      g_merge_expiration_date = merge_expiration_date;
      g_swap_expiration_time = swap_expiration_time ;
      g_event_address = event_address ;
      g_state = STATE_WAITING_FOR_CONFIRMATION ;
      g_deployer_pubkey = deployer_pubkey ;
      IDuneEvents( g_event_address )
        .userSwapDeployed( tvm.pubkey(), s_swap_hash, address(this) );
      IDuneRootSwap( s_root_address )
        .deployedBy( tvm.pubkey(), s_swap_hash, deployer_pubkey );
    }




  function _isRelay( uint256 key ) private view returns ( uint8 index ) {
    index = _isRelayExn(key, EXN_AUTH_FAILED);
  }




  function confirmOrder(
                         string order_id,
                         uint256 hashed_secret ,
                         uint64 ton_amount,
                         address dest,
                         address depool,
                         uint64 swap_expiration_date
                         ) public
  {
    uint8 index = _isRelay( msg.pubkey () );
    require( uint64(now) < g_merge_expiration_date, EXN_SWAP_EXPIRED );
    require( g_state == STATE_WAITING_FOR_CONFIRMATION, EXN_ALREADY_CONFIRMED );
    tvm.accept();
    require( _getSwapHash( tvm.pubkey(),
                           order_id, hashed_secret, ton_amount,
                           dest, depool, swap_expiration_date ) == s_swap_hash,
             EXN_WRONG_SWAP_HASH );

    if( ! g_initialized ){
      g_initialized = true;
      
      g_order_id = order_id ;
      g_hashed_secret = hashed_secret ;
      g_ton_amount = ton_amount ;
      g_dest = dest ;
      g_depool = depool ;
      g_swap_expiration_date = swap_expiration_date + g_swap_expiration_time;

      g_state_count = 0 ;
      g_nconfirms = 0 ;
      g_voteMask = 0 ;
      g_failed_revelations = 0 ;
    }

    // prevent confirmation after expiration date
    require( uint64(now) < g_swap_expiration_date, EXN_SWAP_EXPIRED );

    uint64 bit = uint64(1) << index ;
    require(  ( g_voteMask & bit ) == 0 , EXN_ALREADY_CONFIRMED );
    g_voteMask |= bit ;
    g_nconfirms++ ;

    IDuneEvents( g_event_address )
      .orderConfirmedByRelay( tvm.pubkey(), s_swap_hash,
                              order_id, msg.pubkey() );

    if( g_nconfirms >= g_nreqs &&
        g_state == STATE_WAITING_FOR_CONFIRMATION ) {
      _orderStateChanged( STATE_FULLY_CONFIRMED );
      IDuneRootSwap( s_root_address )
        .deployedConfirmed( tvm.pubkey(), s_swap_hash, g_deployer_pubkey );

      _requestCredit();
    }
  }


  function requestCredit() public
    AuthOwnerOrRelay()
  {
    require( g_state == STATE_CREDIT_DENIED, EXN_UNKNOWN_ORDER );
    require( uint64(now) < g_swap_expiration_date, EXN_SWAP_EXPIRED );
    tvm.accept();

    _requestCredit();
  }



  function _requestCredit() private
  {
    IDuneGiver( s_giver_address ).
      creditOrder(g_order_id,
                  s_swap_hash,
                  g_ton_amount,
                  tvm.pubkey());

    _orderStateChanged( STATE_WAITING_FOR_CREDIT ) ;
  }







  function revealOrderSecret( string secret ) public
    AuthOwnerOrRelay()
  {
    require ( g_state == STATE_CREDITED, EXN_NOT_YET_CREDITED );
    require( g_failed_revelations < MAX_FAILED_REVELATIONS,
             EXN_MAX_FAILED_REVELATIONS );
    tvm.accept();

    uint8 revelation_state ;
    
    if( uint64(now) > g_swap_expiration_date ) {

      revelation_state = REVEL_STATE_EXPIRED ;
      g_failed_revelations = MAX_FAILED_REVELATIONS ;

    } else {

      uint256 computed_hash = sha256( secret.toSlice() );
      if( computed_hash == g_hashed_secret  ){

        revelation_state = REVEL_STATE_SUCCESS ;
        _orderStateChanged( STATE_REVEALED ) ;
        _maybeTransferOrder();

      } else {
        
        revelation_state = REVEL_STATE_FAILED ;
        g_failed_revelations++ ;

      }

    
    } 
    
    IDuneEvents( g_event_address )
      .orderSecretRevealed(tvm.pubkey(),
                           s_swap_hash,
                           g_order_id,
                           secret,
                           revelation_state );
    
  }




  function transferOrder() public AuthOwnerOrRelay()
  {
    require( g_state == STATE_REVEALED ||
             g_state == STATE_DEPOOL_DENIED, EXN_NOT_YET_REVEALED );
    tvm.accept();
    _maybeTransferOrder();
  }

  function cancelOrder() public AuthOwnerOrRelay()
  {
    require( g_state != STATE_WAITING_FOR_CREDIT 
             && g_state != STATE_WAITING_FOR_DEPOOL
             && g_state != STATE_REVEALED
             && g_state != STATE_DEPOOL_DENIED
             // since orders are deleted, no need for:
             //   && g_state != STATE_TRANSFERRED
             //   && g_state != STATE_CANCELLED
             , EXN_NOT_CANCELLABLE );
    require( uint64(now) > g_swap_expiration_date, EXN_NOT_YET_EXPIRED );
    tvm.accept();

    if( g_state == STATE_CREDITED ){

      s_giver_address.transfer({ value: g_ton_amount, bounce: false, flag: 0 });

    }
    _orderStateChanged( STATE_CANCELLED ) ;
  }



  function receiveCredit() public override AuthGiver()
  {
    _orderStateChanged( STATE_CREDITED ) ;
  }






  function creditDenied() public override AuthGiver()
  {
    tvm.accept();
    _orderStateChanged( STATE_CREDIT_DENIED ) ; 
  }



  function _maybeTransferOrder() private {
    
    if ( g_state == STATE_REVEALED ||
         g_state == STATE_DEPOOL_DENIED ){

      if ( g_depool == address(0) ){

        _orderStateChanged( STATE_TRANSFERRED ) ;

        g_dest.transfer({
          value: g_ton_amount, bounce: false, flag: 1 });

      } else {

        _orderStateChanged( STATE_WAITING_FOR_DEPOOL ) ;
        IDePool( g_depool ).addVestingStake {
            flag: 1,
            value: g_ton_amount + DEPOOL_FEE }
        ( g_ton_amount,
          g_dest,
          uint32(MONTH_SECONDS),
          uint32(16) * uint32(MONTH_SECONDS) ); // 16 months
      }
    } 
  }





  function closeSwap() public AuthOwnerOrRelay()
  {
    require( uint64(now) > g_merge_expiration_date, EXN_SWAP_NOT_EXPIRED );
    tvm.accept();
    selfdestruct( s_giver_address );
  }




  function receiveAnswer(uint32 errcode,
                         uint64 //comment
                         ) public override
  {
    require( msg.sender == g_depool , EXN_AUTH_FAILED );
    require( g_state == STATE_WAITING_FOR_DEPOOL,
             EXN_UNEXPECTED_ANSWER_FROM_DEPOOL );

    if ( errcode == 0 ){ // SUCCESS
      _orderStateChanged( STATE_TRANSFERRED ) ;
    } else {
      _orderStateChanged( STATE_DEPOOL_DENIED ) ; 
    }
  }




  function updateRelays( uint256[] relays, uint8[] indexes,
                         uint8 nreqs) public override AuthRoot()
  {
    mapping ( uint256 => uint8 ) map ;
    uint256 len = relays.length;
    for ( uint8 i = 0 ; i < len ; i++ ){
      map[ relays[i] ] = indexes[i] ;
    }
    g_relays = map;
    g_nreqs = nreqs;
  }

  function _orderStateChanged( uint8 state ) private
  {
    g_state = state ;
    g_state_count ++;
    emit OrderStateChanged( state) ;
    IDuneEvents( g_event_address )
      .orderStateChanged
      { value: 0.12 ton }
      (
                         tvm.pubkey(), s_swap_hash, g_order_id,
                         g_state_count, state );
  }


  fallback () external {}
  receive () external {}  // same as default

  


  function get() public view returns
    (
     uint8 nreqs,
     uint256[] relays,
     uint8[] indexes,
     address root_address,
     address giver_address,
     address event_address,
     uint64 merge_expiration_date,
     uint256 swap_hash,
     string order_id,
     uint256 hashed_secret ,
     uint64 ton_amount,
     address dest,
     address depool,
     uint64 swap_expiration_date
     )
  {
    nreqs = g_nreqs;
    optional(uint256,uint8) opt_relay = g_relays.min() ;
    while( opt_relay.hasValue() ){
      (uint256 relay, uint8 index) = opt_relay.get();
      relays.push( relay );
      indexes.push( index );
      opt_relay = g_relays.next(relay);
    }
    root_address = s_root_address ;
    giver_address = s_giver_address ;
    swap_hash = s_swap_hash ;
    event_address = g_event_address ;

    merge_expiration_date = g_merge_expiration_date ;
    
    order_id = g_order_id ;
    hashed_secret = g_hashed_secret ;
    ton_amount = g_ton_amount ;
    dest = g_dest ;
    depool = g_depool ;
    swap_expiration_date = g_swap_expiration_date ;
  }

  function getOrderRemainingTime()
    public view returns ( uint64 remaining_time )
  {
    uint64 now64 = uint64(now);
    if( now64 > g_swap_expiration_date )
      remaining_time = 0 ;
    else
      remaining_time = g_swap_expiration_date - now64 ;
  }

  
  
}

