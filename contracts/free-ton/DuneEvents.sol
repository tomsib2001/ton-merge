/*
  Implementation of contract DuneEvents
 */

pragma ton-solidity >= 0.37.0;

pragma AbiHeader expire;
pragma AbiHeader pubkey;

import "./IDuneEvents.sol";
import "./DuneSwapBase.sol";
import "./DuneHashBase.sol";

contract DuneEvents is IDuneEvents,
  DuneSwapBase, DuneHashBase {

  uint8 constant EXN_SWAP_NOT_EXPIRED = 109 ;
  
  constructor( address giver_address ) public AuthOwner() {
    tvm.accept();
    g_giver_address = giver_address ;
  }

  function init( address root_address, TvmCell duneUserSwapCode,
                 uint64 merge_expiration_date )
    public override AuthGiver()
  {
    g_root_address = root_address ;
    g_duneUserSwapCode = duneUserSwapCode ;
    g_merge_expiration_date = merge_expiration_date ;
  }

  event UserSwapDeployed(uint256 pubkey, uint256 swap_hash, address user_addr);
  event OrderStateChanged(string order_id, uint32 state_count, uint8 state);
  event OrderSecretRevealed( string order_id, string secret, uint8 status );
  event OrderCredited ( string order_id , bool accepted );
  event OrderConfirmedByRelay ( string order_id , uint256 pubkey );
  event RelayPingPong( uint256 pubkey, uint8 kind );
  
  function userSwapDeployed ( uint256 pubkey,
                              uint256 swap_hash,
                              address user_addr
                              ) public override AuthUser(pubkey, swap_hash)
  {
    emit UserSwapDeployed( pubkey, swap_hash, user_addr );
  }

  function orderStateChanged( uint256 pubkey,
                              uint256 swap_hash,
                              string order_id,
                              uint32 state_count,
                              uint8 state
                              ) public override AuthUser(pubkey, swap_hash)
  {
    emit OrderStateChanged( order_id , state_count, state );
  }

  function orderSecretRevealed( uint256 pubkey,
                                uint256 swap_hash,
                                string order_id,
                                string secret,
                                uint8 status
                                ) public override AuthUser(pubkey, swap_hash)
  {
    emit OrderSecretRevealed( order_id, secret, status );
  }

  function orderCredited ( string order_id,
                           bool accepted
                           ) public override AuthGiver()
  {
    emit OrderCredited( order_id , accepted );
  }

  function orderConfirmedByRelay ( uint256 pubkey,
                                   uint256 swap_hash,
                                   string order_id,
                                   uint256 relay_pubkey
                                   ) public override AuthUser(pubkey, swap_hash)
  {
    emit OrderConfirmedByRelay( order_id , relay_pubkey );
  }

  function relayPingPong ( uint256 pubkey, uint8 kind )
    public override AuthRoot()
  {
    emit RelayPingPong( pubkey, kind );
  }
  
  function get() public view returns
    (
     address root_address,
     address giver_address,
     address event_address
     )
  {
    root_address = g_root_address ;
    giver_address = g_giver_address ;
    event_address = address(this) ;
  }


  function closeSwap() public {
    require( uint64(now) > g_merge_expiration_date, EXN_SWAP_NOT_EXPIRED );
    tvm.accept();
    selfdestruct( g_giver_address );
  }
  
}

