pragma ton-solidity >= 0.37.0;

pragma AbiHeader expire;
pragma AbiHeader pubkey;

import "./DuneUserSwap.sol";

abstract contract DuneSwapBase {

  uint8 constant EXN_AUTH_FAILED = 100 ;

  // address of the DuneRootSwap contract
  address public g_root_address ;
  // address of this contract
  address public g_giver_address ;
  // code of DuneUserSwap contract to compute and verify addresses
  TvmCell public g_duneUserSwapCode ;

  // date of expiration for all swaps (one week after Dune's expiration date)
  uint64 public g_merge_expiration_date ;

  modifier AuthOwner() {
    require( msg.pubkey() == tvm.pubkey(), EXN_AUTH_FAILED );
    _;
  }

  modifier AuthRoot() {
    require( msg.sender == g_root_address, EXN_AUTH_FAILED );
    _;
  }

  modifier AuthGiver() {
    require( msg.sender == g_giver_address, EXN_AUTH_FAILED );
    _;
  }

  modifier AuthUser(uint256 pubkey, uint256 swap_hash) {
    address user_addr = _getUserSwapAddress( pubkey, swap_hash );
    require( msg.sender == user_addr, EXN_AUTH_FAILED );
    _;
  }

  function _getUserSwapAddress( uint256 pubkey, uint256 swap_hash )
    internal view returns ( address addr )
  {
    /* Recompute the address that a user with the provided pubkey
       should have, if he uses the same contract and static
       variables. */
    TvmCell stateInit = tvm.buildStateInit({
      contr: DuneUserSwap,
          pubkey: pubkey,
          code: g_duneUserSwapCode,
          varInit: {
             s_giver_address : g_giver_address ,
             s_root_address: g_root_address ,
             s_swap_hash : swap_hash
            }
          });
    addr = address(tvm.hash(stateInit));
  }

  function getUserSwapAddress( uint256 pubkey, uint256 swap_hash  )
    public view returns ( address addr )
  {
    addr = _getUserSwapAddress( pubkey, swap_hash );
  }

  function remainingTime() public view returns (uint64 remaining_time)
  {
    uint64 now64 = uint64(now);
    if(now64 > g_merge_expiration_date){
      remaining_time = 0;
    } else {
      remaining_time = g_merge_expiration_date - now64 ;
    }
  }
}
