/* Interface IDuneEvents */

pragma ton-solidity >= 0.37.0;

interface IDuneEvents {

  function init( address root_address,
                 TvmCell duneUserSwapCode,
                 uint64 expiration_date ) external ;

  function userSwapDeployed ( uint256 pubkey,
                              uint256 swap_hash,
                              address addr
                              ) external ;
  function orderStateChanged( uint256 pubkey,
                              uint256 swap_hash,
                              string order_id,
                              uint32 state_count,
                              uint8 state
                             ) external ;
  function orderSecretRevealed( uint256 pubkey,
                              uint256 swap_hash,
                                string order_id,
                                string secret,
                                uint8 status
                                ) external ;
  function orderCredited ( string order_id,
                           bool accepted
                           ) external ;
  function orderConfirmedByRelay ( uint256 pubkey,
                                   uint256 swap_hash,
                                   string order_id,
                                   uint256 relay_pubkey
                                   ) external ;
  function relayPingPong ( uint256 pubkey, uint8 kind ) external ;

}
