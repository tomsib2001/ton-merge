/* Interface IRootUserWallet */

pragma ton-solidity >= 0.37.0;

interface IDuneRootSwap {

  function getSwapInfo( address event_address ) external ;

  function deployedBy( uint256 pubkey, uint256 swap_hash, uint256 relay_pubkey ) external ;
  function deployedConfirmed( uint256 pubkey, uint256 swap_hash, uint256 relay_pubkey ) external ;
}
