open EzCompat
open Lwt_utils

let string_of_swap s =
  EzEncoding.construct ~compact:true Encoding_common.swap s

let check_pid s =
  let name = s ^ "_pid" in
  let> old_pid = Db.CONFIG.read ~name in
  let old_pid = int_of_string old_pid in
  match Unix.kill old_pid 0 with
  | exception exn ->
    Printf.eprintf "former %s with pid %d is dead (exn %s)\n%!"
      s old_pid ( Printexc.to_string exn );
    Lwt.return_true
  | _ ->
    Printf.eprintf "%s is already running with pid %d\n%!" s old_pid;
    Lwt.return_false

let register_pid s =
  let name = s ^ "_pid" in
  let value = string_of_int @@ Unix.getpid () in
  let> () = Db.CONFIG.update ~name ~value in
  Lwt.return_unit


let map_of_json ?(root=[]) json =
  let json = Ezjsonm.from_string json in
  let map = ref StringMap.empty in
  let add path s =
    let key = String.concat ":" ( List.rev path ) in
    map := StringMap.add key s !map
  in
  let rec iter path json =
    add path json;
    match json with
      `O list ->
        List.iter (fun (s,v) ->
            iter (s :: path) v
          ) list
    | `A list ->
        List.iteri (fun i v ->
            iter (string_of_int i :: path) v
          ) list
    | `Bool _
    | `Null
    | `Float _
    | `String _ -> ()
  in
  iter (List.rev root) json;
  !map

let fatal loc =
  Printf.eprintf "Fatal error at %s\n%!" loc;
  exit 2

let todo loc =
  Printf.eprintf "Fatal error TODO at %s\n%!" loc;
  exit 2
