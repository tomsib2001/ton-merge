
open EzCompat
open Ton_sdk
open Data_types

open Lwt_utils

type swap_info = {
  mutable swap : swap ;
  mutable swap_last_getSwapAddress : float ;
  mutable swap_last_deployUserSwap : float ;
  mutable swap_last_confirmOrder : float ;
  mutable swap_last_revealOrderSecret : float ;
  mutable swap_last_requestCredit : float ;
  mutable swap_last_cancelOrder : float ;
  mutable swap_address : string option ;
}

type address_info = {
  address_swap_hash : string ;
  address_address : string ;
  address_last_update : float ;
}

type confirmations = {
  mutable relay_pubkeys : StringSet.t ;
}

let last_confirmation_serial = ref 0l
let last_contracts_serial = ref 0l
let last_logical_time = ref 1L

let swaps_table = Hashtbl.create 711
let addresses_table = Hashtbl.create 711
let confirmations_table = Hashtbl.create 711

let update_logical_time logical_time =
  if logical_time > !last_logical_time then
    last_logical_time := logical_time

(* Load everything that changed in the database since the last
   logical_time, swaps are saved in the swaps_table and contract
   addresses in the addresses_table. *)
let update_swaps () =
  let> logical_time = Db.get_logical_time () in
  let rec iter () =
    if !last_logical_time < logical_time then

      let last_logical_time = !last_logical_time in
      Printf.eprintf "update from db\n%!";
      let> swaps = Db.SWAPS.list ~logical_time:last_logical_time () in
      List.iter (fun swap ->
          update_logical_time swap.logical_time;
          match Hashtbl.find swaps_table swap.swap_id with
          | exception Not_found ->
            let ss = {
              swap ;
              swap_last_getSwapAddress = 0. ;
              swap_last_deployUserSwap = 0. ;
              swap_last_confirmOrder = 0. ;
              swap_last_revealOrderSecret = 0. ;
              swap_last_requestCredit = 0. ;
              swap_last_cancelOrder = 0. ;
              swap_address = None ;
            } in
            Printf.eprintf "New swap: %d\n%!" swap.swap_id ;
            Hashtbl.add swaps_table swap.swap_id ss
          | ss ->
            Printf.eprintf "Updated swap: %d (dune:%s freeton:%s)\n%!"
              swap.swap_id
              ( Encoding_common.string_of_dune_status swap.dune_status )
              ( Encoding_common.string_of_freeton_status swap.freeton_status );
            ss.swap <- swap;
            (* state changed, reset query timer *)
            ss.swap_last_revealOrderSecret <- 0.
        ) swaps;

      (* We iterate until last_logical_time = logical_time, which should
         happen when we loaded old the modified records in ascending order. *)
      iter ()

    else
      Lwt.return_unit

  in
  let> () = iter () in



  let rec iter () =

    let> addresses = Db.CONTRACTS.list ~serial:!last_contracts_serial in
    if addresses != [] then
      let address_last_update = !Freeton.curtime in
      List.iter (fun (address_swap_hash, address_address, serial) ->

          if serial > !last_contracts_serial then
            last_contracts_serial := serial;

          match Hashtbl.find addresses_table address_swap_hash with
          | exception Not_found ->
            Printf.eprintf "New contract: %s -> %s\n%!"
              address_swap_hash address_address ;
            Hashtbl.add addresses_table address_swap_hash
              { address_swap_hash ;
                address_address ;
                address_last_update ;
              }

          | old_address ->
            Printf.eprintf "FATAL: address for %s changed %s -> %s\n%!"
              address_swap_hash old_address.address_address
              address_address
        ) addresses ;
      iter ()
    else
      Lwt.return_unit

  in
  let> () = iter () in

  let rec iter () =

    let> confs = Db.CONFIRMATIONS.list ~serial:!last_confirmation_serial in

    List.iter (fun (serial, swap_id, relay_pubkey) ->

        if serial > !last_confirmation_serial then
          last_confirmation_serial := serial ;

        Printf.eprintf "New confirm %ld: %d -> %s \n%!"
          serial swap_id relay_pubkey;

        let conf =
          match Hashtbl.find confirmations_table swap_id with
          | exception Not_found ->
            let conf = {
              relay_pubkeys = StringSet.empty ;
            } in
            Hashtbl.add confirmations_table swap_id conf ;
            conf
          | conf -> conf
        in
        conf.relay_pubkeys <- StringSet.add relay_pubkey conf.relay_pubkeys
      ) confs ;

    if confs != [] then
      iter ()
    else
      Lwt.return_unit
  in
  iter ()

let is_expired config ss =
  let is_expired =
    let swap_time = ss.swap.time in
    let swap_time = CalendarLib.Calendar.to_unixfloat swap_time in
    !Freeton.curtime > swap_time +. Int64.to_float config.swap_expiration_time
  in
  if is_expired then
    match ss.swap.freeton_status with
    | SwapWaitingForCredit
    | SwapWaitingForDepool
    | SwapRevealed
    | SwapDepoolDenied
    | SwapCancelled
    | SwapTransferred -> false
    | _ -> true
  else
    false

let monitor ?resend_delay config =

  let resend_delay = match resend_delay with
    | None -> config.resend_delay
    | Some resend_delay -> resend_delay in
  let keypair = match Freeton.keypair with
    | None -> failwith "You must provide a passpharse with TON_MERGE_PASSPHRASE"
    | Some keypair -> keypair
  in
  let client = CLIENT.create config.network_url in
  let server_url = config.network_url in
  let root_address = config.root_address in

  if keypair.public <> config.relay_pubkey then begin
    Printf.eprintf "TON_MERGE_PASSPHRASE differs from pubkey\n%!";
    exit 2
  end ;

  let> result =
    Ton_sdk.ACTION.call_lwt ~client ~server_url
      ~address:config.giver_address ~abi:Freeton.abi_DuneGiver
      ~meth:"getTime" ~params:"{}"
      ~local:true ()
  in
  begin
    match result with
    | Error exn ->
      Printf.eprintf "getTime failed. Aborting:\n%s\n%!"
        (Printexc.to_string exn);
      exit 2
    | Ok json ->
      let map = Relay_misc.map_of_json json in
      let time = StringMap.find "time" map in
      match time with
      | `String s ->
        let time = float_of_string s in
        Freeton.set_time time
      | _ -> assert false
  end;

  let manage_swap giver_balance ss =
    let goodtime = !Freeton.curtime -. resend_delay in
    match ss.swap.swap_hash with
    | None ->
      Printf.eprintf "hash?%!";
      if goodtime > ss.swap_last_getSwapAddress then begin
        Printf.eprintf "ask%!";
        let> res = Freeton.get_swap_hash ~client ~config ss.swap in
        ss.swap_last_getSwapAddress <- !Freeton.curtime;
        match res with
        | None -> Lwt.return_unit
        | Some swap_hash ->
          ss.swap.swap_hash <- Some swap_hash ;
          let swap_id = ss.swap.swap_id in
          Db.SWAPS.set_swap_hash ~swap_id swap_hash
      end else begin
        Printf.eprintf "wait%!";
        Lwt.return_unit
      end

    | Some swap_hash ->

      begin
        Printf.eprintf "address?%!";
        match ss.swap_address with
        | Some _ -> Printf.eprintf "!%!"
        | None ->
          match Hashtbl.find addresses_table swap_hash with
          | exception Not_found -> Printf.eprintf "no%!"
          | address -> ss.swap_address <- Some address.address_address
      end ;

      if is_expired config ss then begin
        Printf.eprintf "expired!%!";

        match ss.swap_address with
        | None -> Lwt.return_unit
        | Some address ->
          if goodtime > ss.swap_last_cancelOrder then

            let> result =
              Ton_sdk.ACTION.call_lwt ~client ~server_url
                ~address ~abi:Freeton.abi_DuneUserSwap
                ~meth:"cancelOrder" ~params:"{}"
                ~keypair ~local:false ()
            in
            ss.swap_last_cancelOrder <- !Freeton.curtime;
            begin
              match result with
              | Ok _ ->
                Printf.eprintf "cancelOrder %d sent\n%!" ss.swap.swap_id
              | Error exn ->
                Printf.eprintf "cancelOrder %d failed: %s\n%!"
                  ss.swap.swap_id ( Printexc.to_string exn )
            end;
            Lwt.return_unit
          else
            Lwt.return_unit

      end else begin
        Printf.eprintf "%s %!"
          (Encoding_common.string_of_freeton_status ss.swap.freeton_status);
        match ss.swap.freeton_status with

        | SwapWaitingForConfirmation ->

          (*
          Printf.eprintf "Check confirmed swap: %s\n%!"
            ( Relay_misc.string_of_swap ss.swap );
*)
          Printf.eprintf "waiting!%!";
          begin
            match ss.swap_address with

            | None ->
              if goodtime < ss.swap_last_deployUserSwap then begin
                Printf.eprintf "wait-to-deploy%!" ;
                Lwt.return_unit
              end else begin
                match ss.swap.dune_status with
                | SwapSubmitted
                | SwapCanReveal
                | SwapCompleted
                | SwapExpired
                | SwapRefundAsked
                | SwapRefunded ->
                  Printf.eprintf "dune-not-ready%!" ;
                  Lwt.return_unit (* nothing to do *)
                | SwapConfirmed ->
                  Printf.eprintf "ask-deploy%!";

                  let params = Printf.sprintf
                      {|{ "pubkey": "0x%s", "swap_hash": "0x%s" }|}
                      ss.swap.freeton_pubkey swap_hash
                  in
                  ss.swap_last_deployUserSwap <- !Freeton.curtime;
                  let> result =
                    Ton_sdk.ACTION.call_lwt ~client ~server_url
                      ~address:root_address ~abi:Freeton.abi_DuneRootSwap
                      ~meth:"deployUserSwap" ~params
                      ~keypair ~local:false ()
                  in
                  begin
                    match result with
                    | Error exn ->
                      Printf.eprintf "Error deploy for pubkey %s: %s\n%!"
                        ss.swap.freeton_pubkey (Printexc.to_string exn) ;
                    | Ok res ->
                      Printf.eprintf "Deployment asked for pubkey %s: returned %s\n%!"
                        ss.swap.freeton_pubkey res;
                  end;
                  Lwt.return_unit
              end ;
            | Some address ->

              if goodtime < ss.swap_last_confirmOrder then begin
                Printf.eprintf "wait-confirm%!" ;
                Lwt.return_unit
              end else
                let need_confirmation =
                  match Hashtbl.find confirmations_table ss.swap.swap_id with
                  | exception Not_found -> true
                  | { relay_pubkeys } ->
                    not ( StringSet.mem config.relay_pubkey relay_pubkeys )
                in
                if need_confirmation then begin
                  Printf.eprintf "need-confirm%!";
                  let params = Freeton.params_of_swap ss.swap in
                  ss.swap_last_confirmOrder <- !Freeton.curtime;
                  let> result =
                    Ton_sdk.ACTION.call_lwt ~client ~server_url
                      ~address ~abi:Freeton.abi_DuneUserSwap
                      ~meth:"confirmOrder" ~params
                      ~keypair ~local:false ()
                  in
                  begin
                    match result with
                    | Error exn ->
                      Printf.eprintf "Error confirmOrder for pubkey %s: %s\nparams: %s\n%!"
                        ss.swap.freeton_pubkey (Printexc.to_string exn) params;
                    | Ok res ->
                      Printf.eprintf "confirmOrder asked for swap %d: returned %s\n%!"
                        ss.swap.swap_id res;
                  end;
                  Lwt.return_unit
                end else begin
                  Printf.eprintf "has-confirmed%!";

                  Lwt.return_unit
                end
          end

        | SwapFullyConfirmed ->
          (* wait for credit. TODO: If too long, use requestCredit() ? *)
          Lwt.return_unit

        | SwapWaitingForCredit ->
          (* wait for credit *)
          Lwt.return_unit

        | SwapCreditDenied ->

          if !giver_balance > ss.swap.ton_amount &&
             goodtime > ss.swap_last_requestCredit then begin

            match ss.swap_address with
            | None -> Lwt.return_unit
            | Some address ->
              ss.swap_last_requestCredit <- !Freeton.curtime;
              let> result =
                Ton_sdk.ACTION.call_lwt ~client ~server_url
                  ~address ~abi:Freeton.abi_DuneUserSwap
                  ~meth:"requestCredit" ~params:"{}"
                  ~keypair ~local:false ()
              in
              (* We don't really care about the result, we will learn later if
                 it was successful. *)
              begin
                match result with
                | Ok _ ->
                  giver_balance := Z.sub !giver_balance ss.swap.ton_amount
                | _ -> ()
              end;
              Lwt.return_unit
          end else
            Lwt.return_unit

        | SwapCredited ->
          begin
            match ss.swap.secret with
            | None ->
              Lwt.return_unit
            | Some secret ->
              if !giver_balance > ss.swap.ton_amount &&
                 goodtime > ss.swap_last_revealOrderSecret then begin

                match ss.swap_address with
                | None -> Lwt.return_unit
                | Some address ->
                  let params = Printf.sprintf
                      {|{ "secret": "%s" }|}
                      (Freeton.json_of_string (Bytes.to_string secret))
                  in
                  ss.swap_last_revealOrderSecret <- !Freeton.curtime;
                  let> result =
                    Ton_sdk.ACTION.call_lwt ~client ~server_url
                      ~address ~abi:Freeton.abi_DuneUserSwap
                      ~meth:"revealOrderSecret" ~params
                      ~keypair ~local:false ()
                  in
                  begin
                    match result with
                    | Ok _ ->
                      Printf.eprintf "revealOrderSecret %d sent\n%!"
                        ss.swap.swap_id
                    | Error exn ->
                      Printf.eprintf "revealOrderSecret %d failed: %s\n%!"
                        ss.swap.swap_id ( Printexc.to_string exn )
                  end;
                  Lwt.return_unit

              end
              else
                Lwt.return_unit
          end

        | SwapRevealed ->
          Lwt.return_unit

        | SwapWaitingForDepool ->
          Lwt.return_unit

        | SwapDepoolDenied ->
          (* TODO: we should warn the user, he should declare the donor or change
             the depool *)
          Lwt.return_unit

        | SwapTransferred -> (* we are done, wait for Dune side *)
          begin
            match ss.swap.dune_status with
            | SwapCompleted ->
              Hashtbl.remove swaps_table ss.swap.swap_id;
              Lwt.return_unit
            | _ ->
              Lwt.return_unit
          end

        | SwapCancelled -> (* we are done, wait for Dune side *)
          begin
            match ss.swap.dune_status with
            | SwapRefunded ->
              Hashtbl.remove swaps_table ss.swap.swap_id;
              Lwt.return_unit
            | _ ->
              Lwt.return_unit
          end
      end
  in

  let rec iter () =
    let> () = Lwt_unix.sleep 3.0 in
    Printf.eprintf ".%!";

    Freeton.update_curtime ();

    let> () = Freeton.maybe_ping ~client ~config ~keypair 0 in

    let> () = update_swaps () in

    let swaps = ref [] in
    Hashtbl.iter (fun _ ss ->
        swaps := ss :: !swaps
      ) swaps_table ;

    let> giver_balance =
      let> result = REQUEST.post_lwt config.network_url
          (REQUEST.account ~level:1 config.giver_address) in
      match result with
      | Ok [ acc ] -> begin
          match acc.acc_balance with
          | None -> Lwt.return Z.zero
          | Some z -> Lwt.return z
        end
      | Ok [] ->
        Printf.eprintf "Giver disappeared!%!";
        Lwt.return Z.zero
      | Ok _ -> assert false
      | Error exn ->
        Printf.eprintf "Error while requesting giver_balance (using 0): %s\n%!" (Printexc.to_string exn);
        Lwt.return Z.zero

    in
    let giver_balance = ref giver_balance in

    let> () = Lwt_list.iter_s (fun s ->
        Printf.eprintf "|%!";
        let> () = manage_swap giver_balance s in
        Printf.eprintf "\n%!";
        Lwt.return_unit
      ) !swaps in

    iter ()
  in
  Printf.eprintf "Enter main loop...\n%!";
  iter ()
