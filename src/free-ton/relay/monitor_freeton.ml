
open EzCompat
open Ton_sdk
open Data_types
open Lwt_utils

let fatal = Relay_misc.fatal

let wait_before_retry delay =
  Lwt_unix.sleep (float_of_int delay)

let iter_event ~network_url ~abi ~client ~msg_id f =
  let rec iter () =
    let> result = REQUEST.post_lwt network_url
        (REQUEST.messages ~level:3 ~id:msg_id []) in
    match result with
    | Error exn ->
      Printf.eprintf "iter_message: exception %s\n%!"
        (Printexc.to_string exn);
      let> () = wait_before_retry 3 in
      iter ()
    | Ok [ msg ] ->
      Printf.printf "  MESSAGE: %s\n%!"
        ( ENCODING.string_of_message msg ) ;
      begin (* decode_message only works when there is a msg_body too *)
        match msg.msg_body with
        | None -> Lwt.return_unit
        | Some _body ->
          match msg.msg_boc with
          | None -> fatal __LOC__
          | Some boc ->
            match BLOCK.decode_message_boc ~client ~boc ~abi with
            | decoded ->
              if decoded.body_type = 3 (* Event *) then
                f decoded.body_name decoded.body_args
              else
                Lwt.return_unit
            | exception _exn ->
              fatal __LOC__
      end
    | _ -> fatal __LOC__
  in
  iter ()

let rec iter_transactions ~block_id ~network_url ~address f =
  let> res =
    REQUEST.post_lwt network_url
      (REQUEST.transactions
         ~level:3
         ~block_id
         ~account_addr:address [])
  in
  match res with
  | Ok [] -> Lwt.return_unit
  | Ok trs ->
    Printf.eprintf "In block with id: %S\n%!" block_id;
    Lwt_list.iter_s (fun tr ->
        Printf.eprintf "\nTRANSACTION: %s\n%!"
          (ENCODING.string_of_transaction tr);
        f tr) trs
  | Error _ ->
    let> () = wait_before_retry 3 in
    iter_transactions ~block_id ~network_url ~address f

let timeout = 2_000_000_000L

let iter_blocks ~client ~block_id ~address f =
  let rec iter block_id =
    match BLOCK.wait_next_block
            ~client ~block_id ~address
            ~timeout () with
    | b ->
      let block_id = b.id in
      Printf.eprintf "new blockid: %S\n%!" b.id;
      Printf.eprintf "block = %s\n%!"
        (Ton_sdk.TYPES.string_of_block b) ;
      let> () = f b in
      iter block_id
    | exception exn ->
      Printf.eprintf "Exception wait_next_block: %s\n%!"
        ( Printexc.to_string exn );
      let> () = wait_before_retry 3 in
      iter block_id
  in
  iter block_id


let monitor config =

  let keypair = match Freeton.keypair with
    | None -> failwith "You must provide a passpharse with TON_MERGE_PASSPHRASE"
    | Some keypair -> keypair
  in
  let network_url = config.network_url in
  let client = Ton_sdk.CLIENT.create network_url in
  let address = config.event_address in
  let abi = Freeton.abi_DuneEvents in

  let handle_event event_name event_args =
    Freeton.update_curtime ();
    let event_args = match event_args with
      | None -> "{}"
      | Some args -> args in
    Printf.eprintf "*\n**\n***\n\n\n";
    Printf.eprintf "EVENT: %s %s\n\n\n\n%!" event_name event_args;
    let args = Relay_misc.map_of_json event_args in
    let> swap_id =
      match event_name with

      | "UserSwapDeployed" ->
        let pubkey = StringMap.find "pubkey" args in
        let swap_hash = StringMap.find "swap_hash" args in
        let user_addr = StringMap.find "user_addr" args in
        begin
          match pubkey, swap_hash, user_addr with
          | `String pubkey, `String swap_hash, `String user_addr ->
            let pubkey = Freeton.pubkey_of_json pubkey in
            let swap_hash = Freeton.uint256_of_json swap_hash in
            Printf.eprintf "EVENT UserSwapDeployed(%s, %s, %s)\n%!"
              pubkey swap_hash user_addr ;

            let> () = Db.CONTRACTS.set ~swap_hash ~address:user_addr in

            Lwt.return_none

          | _ -> fatal __LOC__
        end

      | "OrderStateChanged" ->
        let order_id = StringMap.find "order_id" args in
        let state_count = StringMap.find "state_count" args in
        let state = StringMap.find "state" args in
        begin
          match order_id, state_count, state with
          | `String order_id,
            `String state_count,
            `String state ->
            let swap_id =
              Freeton.swap_id_of_json order_id in
            let state_count = Int32.of_string state_count in
            Printf.eprintf
              "EVENT OrderStateChanged(%d, %ld, %s)\n%!"
              swap_id state_count state ;

            let> res =
              Db.SWAPS.get_freeton_status ~swap_id in

            let old_state_count = match res with
                Some ( old_state_count, _old_status ) -> old_state_count
              | None -> 0l
            in

            let> () =
              if old_state_count < state_count then
                Db.SWAPS.set_freeton_status ~swap_id
                  state_count
                  ( match state with
                    | "0" -> SwapWaitingForConfirmation
                    | "1" -> SwapFullyConfirmed
                    | "2" -> SwapWaitingForCredit
                    | "3" -> SwapCreditDenied
                    | "4" -> SwapCredited
                    | "5" -> SwapRevealed
                    | "6" -> SwapWaitingForDepool
                    | "7" -> SwapDepoolDenied
                    | "8" -> SwapTransferred
                    | "9" -> SwapCancelled
                    | _ ->
                      Printf.eprintf "Unexpected freeton status %S\n%!" state;
                      fatal __LOC__
                  )
              else
                Lwt.return_unit
            in
            Lwt.return_some swap_id

          | _ -> fatal __LOC__
        end

      | "OrderCredited" -> (* sent by Giver *)
        let order_id = StringMap.find "order_id" args in
        let accepted = StringMap.find "accepted" args in
        begin
          match order_id, accepted with
            `String order_id, `Bool accepted ->
            let swap_id =
              Freeton.swap_id_of_json order_id in
            Printf.eprintf
              "EVENT OrderCredited(%d, %b)\n%!"
              swap_id accepted ;
            Lwt.return_some swap_id
          | _ -> fatal __LOC__
        end

      | "OrderSecretRevealed" ->
        let order_id = StringMap.find "order_id" args in
        let secret = StringMap.find "secret" args in
        let status = StringMap.find "status" args in
        begin
          match order_id, secret, status with
            `String order_id, `String secret, `String status ->
            let status = int_of_string status in
            let swap_id =
              Freeton.swap_id_of_json order_id in
            let secret = Freeton.string_of_json secret in
            Printf.eprintf
              "EVENT OrderSecretRevealed(%d, %S, %d)\n%!"
              swap_id secret status ;

            let> () =
              if status = 1 then (* success *)
                let> is_good_secret =
                  Db.SWAPS.set_secret ~swap_id (Bytes.of_string secret)
                in
                assert is_good_secret;
                Lwt.return_unit
              else (* expired or failed *)
                Lwt.return_unit
            in
            Lwt.return_some swap_id

          | _ -> fatal __LOC__
        end

      | "OrderConfirmedByRelay" ->
        let order_id = StringMap.find "order_id" args in
        let relay_pubkey = StringMap.find "pubkey" args in
        begin
          match order_id, relay_pubkey with
            `String order_id, `String relay_pubkey ->
            let swap_id =
              Freeton.swap_id_of_json order_id in
            let relay_pubkey =
              Freeton.pubkey_of_json relay_pubkey in
            Printf.eprintf
              "EVENT OrderConfirmedByRelay(%d, %s)\n%!"
              swap_id relay_pubkey ;

            let> () = Db.CONFIRMATIONS.add ~swap_id relay_pubkey in
            Lwt.return_some swap_id

          | _ -> fatal __LOC__
        end

      | "RelayPingPong" ->
        let pubkey = StringMap.find "pubkey" args in
        let kind = StringMap.find "kind" args in
        begin
          match kind, pubkey with
          | `String kind, `String pubkey ->
            let pubkey = Freeton.pubkey_of_json pubkey in
            let time = Int64.of_float !Freeton.curtime in
            let kind = Int32.of_string kind in
            let> () = Db.PINGS.add ~pubkey ~kind ~time in
            Lwt.return_none
          | _ -> fatal __LOC__
        end

      | _ ->
        Lwt.return_none
    in
    Db.EVENTS.add ?swap_id Db.EVENTS.{ name = event_name ;
                                       args = event_args }
  in
  let> () = Freeton.maybe_ping ~client ~config ~keypair 1 in
  iter_blocks
    ~block_id:config.last_blockid
    ~client
    ~address
    (fun b ->
       let> () =
         iter_transactions
           ~block_id:b.id
           ~network_url
           ~address
           (fun tr ->
              Lwt_list.iter_s (fun msg_id ->
                  iter_event ~abi ~network_url ~client ~msg_id handle_event
                )
                tr.tr_out_msgs
           )
       in
       let> () = Db.CONFIG.set_last_blockid b.id in
       Freeton.maybe_ping ~client ~config ~keypair 1
    )
