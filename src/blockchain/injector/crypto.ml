
open MCrypto
open Data_types
open Hacl

module Public_key_hash = struct
  type t = Bigstring.t

  let to_b58 v = Pkh.b58enc v

  let of_b58 s = Pkh.b58dec s

end

module Public_key = struct
  type t = public Sign.key

  let to_raw_string s = Bigstring.to_string (Sign.unsafe_to_bytes s)

  let of_raw_string_opt s =
    if String.length s < Sign.pkbytes then None
    else
      let pk = Bigstring.create Sign.pkbytes in
      Bigstring.blit_of_string s 0 pk 0 Sign.pkbytes ;
      Some (Sign.unsafe_pk_of_bytes pk)

  let size = Sign.pkbytes

  let hash v : Public_key_hash.t =
    Pk.hash (Sign.unsafe_to_bytes v)

  let to_b58 v = Pk.b58enc (Sign.unsafe_to_bytes v)

  let of_b58 s = Sign.unsafe_pk_of_bytes (Pk.b58dec s)

end

module Secretbox = struct
  include Secretbox

  let box_noalloc key nonce msg = box ~key ~nonce ~msg ~cmsg:msg

  let box_open_noalloc key nonce cmsg = box_open ~key ~nonce ~cmsg ~msg:cmsg

  let box key msg nonce =
    let msglen = String.length msg in
    let cmsg = Bigstring.make (msglen + zerobytes) '\x00' in
    Bigstring.blit_of_string msg 0 cmsg zerobytes msglen ;
    box ~key ~nonce ~msg:cmsg ~cmsg ;
    Bigstring.sub cmsg boxzerobytes (msglen + zerobytes - boxzerobytes)

  let box_open key cmsg nonce =
    let cmsglen = Bigstring.length cmsg in
    let msg = Bigstring.make (cmsglen + boxzerobytes) '\x00' in
    Bigstring.blit cmsg 0 msg boxzerobytes cmsglen ;
    match box_open ~key ~nonce ~cmsg:msg ~msg with
    | false ->
        None
    | true ->
        Some (Bigstring.sub_string msg zerobytes (cmsglen - boxzerobytes))
end

module Signature = struct

  type t = Bigstring.t

  let to_raw_string s = Bigstring.to_string s

  let of_raw_string_opt s =
    if String.length s < Sign.bytes then None
    else
      let sig_ = Bigstring.create Sign.bytes in
      Bigstring.blit_of_string s 0 sig_ 0 Sign.bytes ;
      Some sig_

  let size = Sign.bytes

  let b58enc ?alphabet ?(prefix=Prefix.ed25519_signature) b =
    Base58.encode ?alphabet prefix b
  let b58dec ?alphabet s =
    match Prefix.csig s with
    | Some prefix -> Base58.decode ?alphabet prefix s
    | None -> assert false

  let to_b58 v = b58enc v

  let of_b58 s = b58dec s

end

module Secret_key = struct

  type t = secret Sign.key

  let to_raw_string s = Bigstring.to_string (Sign.unsafe_to_bytes s)

  let of_raw_string_opt s =
    if String.length s < Sign.skbytes then None
    else
      let sk = Bigstring.create Sign.skbytes in
      Bigstring.blit_of_string s 0 sk 0 Sign.skbytes ;
      Some (Sign.unsafe_sk_of_bytes sk)

  let size = Sign.skbytes

  let to_public_key v : Public_key.t = Sign.neuterize v

  let to_b58 v = Sk.b58enc (Sign.unsafe_to_bytes v)

  let of_b58 s = Sign.unsafe_sk_of_bytes (Sk.b58dec s)

  module Encrypted = struct

    let salt_len = 8

    (* Fixed zero nonce *)
    let nonce = Bigstring.make Nonce.bytes '\x00'

    (* Secret keys for Ed25519 are 32 bytes long. *)
    let encrypted_size = Box.boxzerobytes + 32

    let pbkdf ~salt ~password =
      Pbkdf.pbkdf2 ~count:32768 ~dk_len:32l ~salt ~password

    let encrypt ~password sk =
      let salt = Hacl.Rand.gen salt_len in
      let key = Secretbox.unsafe_of_bytes (pbkdf ~salt ~password) in
      let msg = to_raw_string sk in
      Bigstring.concat "" [salt; Secretbox.box key msg nonce]

    let decrypt ~password ~encrypted_sk =
      let salt = Bigstring.sub encrypted_sk 0 salt_len in
      let encrypted_sk = Bigstring.sub encrypted_sk salt_len encrypted_size in
      let key = Secretbox.unsafe_of_bytes (pbkdf ~salt ~password) in
      match Secretbox.box_open key encrypted_sk nonce with
      | None -> None
      | Some s -> of_raw_string_opt s

    let to_raw_string b = Bigstring.to_string b
    let of_raw_string b = Bigstring.of_string b

  end

  let sign_exn ?(watermark=Forge.Watermark.generic) ~sk bytes : Signature.t =
    let msg = Blake2b_32.hash_bytes
        [Bigstring.of_string watermark; bytes] in
    let signature = Bigstring.create 64 in
    Hacl.Sign.sign ~sk ~msg ~signature;
    signature

  let sign ?watermark ~sk bytes =
    try Some (sign_exn ?watermark ~sk bytes)
    with _ -> None

end

let keypair () : Public_key.t * Secret_key.t = Sign.keypair ()

let get_encryption_password () =
  match Sys.getenv_opt "DANAE_PASSWORD" with
  | None | Some "" ->
    failwith "Please define environment variable DANAE_PASSWORD"
  | Some pass -> pass


let generate_encrypted_keys () =
  let pk, sk = keypair () in
  let pkh = Public_key.hash pk in
  let password = get_encryption_password () in
  let enc_sk = Secret_key.Encrypted.encrypt ~password sk in
  {
    pkh = Public_key_hash.to_b58 pkh;
    pk = Some (Public_key.to_b58 pk);
    sk = Encrypted_sk (Secret_key.Encrypted.to_raw_string enc_sk)
  }

let encrypt_keys_with_pass ~password ({ pkh; pk; _} as k) =
  let sk = plain_sk k in
  let sk = Secret_key.of_b58 sk in
  let enc_sk = Secret_key.Encrypted.encrypt ~password sk in
  { pkh; pk;
    sk = Encrypted_sk (Secret_key.Encrypted.to_raw_string enc_sk) }

let encrypt_keys k =
  let password = get_encryption_password () in
  encrypt_keys_with_pass ~password k

let decrypt_keys_with_pass ~password ({ pkh; pk; _} as k) =
  let sk = encrypted_sk k in
  let enc_sk = Secret_key.Encrypted.of_raw_string sk in
  let plain_sk =
    match Secret_key.Encrypted.decrypt ~password ~encrypted_sk:enc_sk with
    | None -> failwith "Invalid password to decrypt secret key"
    | Some sk -> sk in
  { pkh; pk;
    sk = Plain_sk (Secret_key.to_b58 plain_sk) }

let decrypt_keys k =
  let password = get_encryption_password () in
  decrypt_keys_with_pass ~password k

let sign ?watermark ~key msg =
  let sk = Secret_key.of_b58 (plain_sk key) in
  Secret_key.sign ?watermark ~sk msg
