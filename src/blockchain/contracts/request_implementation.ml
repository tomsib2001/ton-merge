open Lwt.Infix
open Metal.Types

module Node = Metal.Node.Make(EzCurl_lwt)

module R = struct

  let debug_flag = ref true

  type url = unit -> EzAPI.base_url Lwt.t

  type 'a res =
    ('a, Json_repr.ezjsonm) EzRequest_lwt.api_result Lwt.t

  type ('a,'b) post_request = 'b res

  type 'a get_request = 'a res

  type spice_operation = {
    src : string;
    op :
      (notif_manager_info,
       (Dune.Types.account_hash *
        (Bigstring.t -> (Bigstring.t, MMisc.metal_error) result Lwt.t))
         option manager_details)
        manager_operation;
  }

  type send_prerequest = spice_operation
  type originate_prerequest = spice_operation

  let error = EzAPI.Err.Case
                {code=500;
                 name="Blockchain error";
                 encoding=Json_encoding.any_ezjson_value;
                 select = (fun x -> Some x);
                 deselect = fun x -> x}

  let output_debug s = Lwt_utils.debug !debug_flag "Spice: %s@." s

  let post url (aenc : 'a Json_encoding.encoding) (benc : 'b Json_encoding.encoding) (suburl : string) v : ('a, 'b) post_request =
    url () >>= fun url ->
    let service =
      EzAPI.post_service
        ~register:false
        ~input:aenc
        ~output:benc
        ~errors:[error]
        EzAPI.Path.(root // suburl)
    in
    EzCurl_lwt.post0
      url
      ~input:v
      service

  let get url benc s =
    url () >>= fun url ->
    let service =
      EzAPI.service
        ~register:false
        ~output:benc
        ~errors:[error]
        EzAPI.Path.(root // s)
    in
    EzCurl_lwt.get0
      url
      service

  let bigmap_getter url (bm : ('a,'b) Json_utils.bigmap) aenc benc a : ('a,'b) post_request =
    match bm with
    | Some id ->
       let block = "head" in
       let service =
         EzAPI.post_service
           ~register:false
           ~input:(Json_utils.dune_expr aenc)
           ~output:(Json_utils.(
               enc_or_null_encoding_as_option (dune_expr benc)))
           ~errors:[error]
           EzAPI.Path.(root //
                         "/chains/main/blocks/" //
                         block //
                         "/context/big_maps/" //
                         (Z.to_string id))
       in
       url () >>= fun url ->
       EzCurl_lwt.post0
         url
         ~input:a
         service
    | _ -> failwith (Printf.sprintf "Empty bigmap: None")

  let make_entrypoint_call ~src ~destination ~amount ~parameter ~entrypoint : spice_operation =
    Format.eprintf "Make entrypoint call %s from %s to %s, parameter : %s@."
      entrypoint src destination parameter;
    { src;
      op = {
        mo_det = TraDetails
            {
              trd_dst = destination;
              trd_amount = amount;
              trd_parameters =
                Some
                  (Some entrypoint,
                   Some parameter);
              trd_collect_call = false;
            };
        mo_info = {
          not_mi_fee=None;
          not_mi_gas_limit=None;
          not_mi_storage_limit=None
        }
      }
    }

  let make_origination ~src ~amount ~code ~parameter =
    { src;
      op = {
        mo_det = OriDetails
            {
              ord_kt1 = None;
              ord_balance = amount;
              ord_delegate = None;
              ord_script =
                Some
                  (Some code,
                   parameter, (* initial storage *)
                   None (*code_has*));
            };
        mo_info = {
          not_mi_fee=None;
          not_mi_gas_limit=None;
          not_mi_storage_limit=None
        }
      }
    }

  let send ~src ~destination ~amount ~parameter ~entrypoint =
    make_entrypoint_call ~src ~destination ~amount ~parameter ~entrypoint

  let originate ~src_lang ~src ~amount ~code ~parameter =
    make_origination ~src ~amount ~code ~parameter

  let to_string enc v =
    Json_encoding.construct enc v
    |> Ezjsonm.value_to_string
    |> Printf.sprintf "{\"dune_expr\":%s}"

end

module R_check___ = (R : Request_utils.REQUEST)
