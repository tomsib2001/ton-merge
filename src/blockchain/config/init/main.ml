let file = ref None
let action = ref None

let get () =
  Config.read_from_db ()
  |> Lwt_main.run
  |> Config.to_string
  |> print_endline

let set file =
  match file with
  | None -> failwith "Action set expects a file"
  | Some f ->
    Config.read_from_file f
    |> Config.write_to_db
    |> Lwt_main.run

let () =
  Arg.parse []
 (fun s -> match !action with
  | None -> action := Some s
  | Some "set" -> file := Some s
  | Some "get" -> failwith "action get has no argument"
  | Some a -> failwith @@ "Unknown action " ^ a
 )
 "Config initializer\nUsage:\nconfig-init <get | set file.conf>";
  match !action with
  | None -> failwith "No action, see usage"
  | Some "get" -> get ()
  | Some "set" -> set !file
  | Some a -> failwith @@ "Unknown action " ^ a
