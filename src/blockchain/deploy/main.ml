
let config_f = ref ""
let force = ref false

let () =
  Arg.parse [
    "--force", Arg.Set force, "Force redeploy even if contract already known"
  ] (fun s -> config_f := s)
    "Dune TON swapper deployer\nUsage:\n\tton-merge-swap-deploy <config.json>";
  try
    Lwt_main.run @@
    Deploy.deploy ~force:!force !config_f
  with
    Failure s ->
    Format.eprintf "Error: %s@." s;
    exit 2
