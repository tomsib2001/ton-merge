# Dune Network / FreeTON merger

## Smart contracts

### Dune Network

A [contract](contracts/dune/swap.liq) (HTLC like) is deployed on dune
network where users can deposit funds, together with a hidden (hashed)
secret and an address on FreeTON where they want to receive their
corresponding TONs.

After the swap period is over, they must reveal their secret to be
credited to unlock their account on Free TON (see FreeTON contract).
At this time, their tokens are locked in the Dune contract and the
swap is final.

There is a configurable threshold (of committed deposits) that must be
reached for tokens to be swapable (the same threshold must be on both
chain). After the deposit and the swap periods are over, if the
threshold of swapped tokens in not reached (or the swap has not
happened) users can call the smart contract to be refunded.

This contract must be delegated to an active baker (likely one of the
foundation's) to guarantee that the network continues to operate
normally during the swap.

### FreeTON
## Building


### OPAM install and setup

#### Installing opam

Install opam 2 on your system with the following command:

```sh
sh <(curl -sL https://raw.githubusercontent.com/ocaml/opam/master/shell/install.sh)
```

If you are installing and setting up opam for the first time run the
following command to create your `~/.opam` directory:

```
opam init --bare
```

#### Disable Sandboxing

Before installing and compiling all dependencies, you need to disable
the sandbox mode of opam to allow PGOCaml to access Postrges on your
system. Check the file `~/.opam/config`, and comment the lines below
if there are still there:

```conf
wrap-build-commands:
  ["%{hooks}%/sandbox.sh" "build"] {os = "linux" | os = "macos"}
wrap-install-commands:
  ["%{hooks}%/sandbox.sh" "install"] {os = "linux" | os = "macos"}
wrap-remove-commands:
  ["%{hooks}%/sandbox.sh" "remove"] {os = "linux" | os = "macos"}
```

When you finished the compilation, you can reenable the sandboxing
mode of opam.

### OCaml Libraires

Install the OCaml dependencies with

```
make submodule
make build-deps
```

#### Development Dependencies

```
make build-dev-deps
```

### Creating the database

You must have postgresql installed on your machine. When this is the
case, simply run the following to create the database:

```
make create-db
```

### Build

Build the project with

```
make
```

If this stage fails with
> PGOCaml: Could not connect to database
you should make sure you have disabled opam's sandboxing and reinstall
pgocaml.

``` 
opam reinstall pgocaml pgocaml_ppx
```

## Crawler

To run the crawler, you must first write a configuration file like
[this one](config/testnet.json).

Then do the following

1. Initialize the crawler configuration in the database:
   ```bash
   bin/ton-merge-config-init set config/my_config.json
   ```
2. Deploy the contract (`dune-client` must be in your path):
   ```bash
   bin/ton-merge-deploy config/my_config.json
   ```
   ```bash
   ...
   New contract KT1K9R5Dt4TZtgxt8zKV9PkxzB5qmUeHhSaP originated.
   Contract memorized as swap_996989154.
   Swap contract is KT1K9R5Dt4TZtgxt8zKV9PkxzB5qmUeHhSaP, saving to database ...
   Sleeping for 5 s before retrieving storage...
   Sleeping for 5 s before retrieving storage...
   Sleeping for 5 s before retrieving storage...
   Sleeping for 5 s before retrieving storage...
   Sleeping for 5 s before retrieving storage...
   Sleeping for 5 s before retrieving storage...
   First level is 951039, saving to database ...
   Saving contract parameters to database ...

   DONE.
   ```
3. Run the crawler (this should be run as a service with _e.g._ systemd):
   ```bash
   bin/ton-merge-crawler
   ```
   Sample output:
   ```bash
   ...
   Blockchain current level 951039
   Last registered level 0
   Registering forward from 951033 to 951033
   Crawl forward from 951033 to 951033
   request       951033
   register      BMSy1jpmmu at level 951033 [2021-03-09T16:02:13Z] : ok (with 5 operations)
     ↳ registered 0 relevant operations

   request       BLdbuPHsss at level 951039
   register      BLdbuPHsss at level 951039 [2021-03-09T16:05:13Z] : ok (with 6 operations)
     | register  opLLMpAq3ChGF4DVbU47GoZVP58LNFYgpHgYGD8upjRynBJ2peC [0]
     ↳ registered 1 relevant operations
   update chain  BLu5HsPjbt -> BLdbuPHsss
   update chain ok

   head          BLdbuPHsss (x4)
   request       BMDA1yjpX6 at level 951040
   register      BMDA1yjpX6 at level 951040 [2021-03-09T16:05:43Z] : ok (with 6 operations)
     | register  ooePrp183p67Pg5SPnUNymBgtyyuNtjyuv9v49L4JyWgAAfXobP [0]
     ↳ registered 1 relevant operations
   update chain  BLdbuPHsss -> BMDA1yjpX6
       Unconfirmed swaps [1] :
       #1: 1000 DUN  / dn1WFXcAMbXP3K7QaDBzZUhkSSwJref9RUaZ ==> freeton:faseaassdasdo [Hash secret: 5065e21cfeeefbd68372c58166fa44ae8a6595cd4011f06c1b01623270dd0240]
   update chain ok

   head          BMDA1yjpX6 (x5)
   request       BKtcxx3KXB at level 951041
   register      BKtcxx3KXB at level 951041 [2021-03-09T16:06:13Z] : ok (with 6 operations)
     | register  ootqvkvdSwCDsezBCBNYmGiTGy8EtceHYm76sgf6wuqe5dmGCQ6 [0]
     ↳ registered 1 relevant operations
   update chain  BMDA1yjpX6 -> BKtcxx3KXB
       Unconfirmed swaps [1] :
       #2: 100 DUN  / dn1WFXcAMbXP3K7QaDBzZUhkSSwJref9RUaZ ==> freeton:faseaassdasdo [VESTED] [Hash secret: 5065e21cfeeefbd68372c58166fa44ae8a6595cd4011f06c1b01623270dd0240]
   update chain ok
   ```


## Web app

### API server

To run the API server do :

```
bin/ton-merge-server
```

You can use the option `--port` to specify a different port from the
one configured.

You can browse the [documentation of the API here](doc/openapi.json)
or you can view it locally by doing:

```
make view-doc
```
