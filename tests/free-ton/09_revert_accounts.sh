#!/bin/bash

. ./env.sh

FT_SWITCH=${NETWORK}
export FT_SWITCH



for i in 0 1 2 3 4 5 6 7 8 9; do
    
  $FT multisig -a user$i --create
  $FT multisig -a user$i --transfer all --to freeton_giver

done
