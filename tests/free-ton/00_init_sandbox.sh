#!/bin/bash

echo -n sandbox1 > NETWORK

. ./env.sh

$FT switch mainnet || exit 2

FT_SWITCH=${NETWORK}

rm -f *.block *.calc

killall ft
killall freeton-relay

$FT --switch ${NETWORK} node --stop
$FT switch --remove ${NETWORK}
$FT switch --create ${NETWORK} || exit 2

export FT_SWITCH

$FT node --start || exit 2
echo
echo Waiting 10 seconds for warmup
sleep 10

$FT account --create freeton_giver --surf --passphrase "%{env:FTGIVER_PASSPHRASE}" || exit 2
$FT node --give freeton_giver:${ADMIN_TONS} || exit 2


export ADMIN_PASSPHRASE="race hospital smart require radar sunny trim bike receive fashion disease unable"

$FT account --create admin --surf --passphrase "%{env:ADMIN_PASSPHRASE}" || exit 2
$FT multisig -a freeton_giver --transfer 1 --to admin --parrain || exit 2
$FT multisig -a admin --create || exit 2


export FTGIVER_PASSPHRASE="dolphin mansion aunt water put wealth crisp invite smoke nasty myth lens"


# $FT node --give freeton_giver:95000 || exit 2

$FT account --delete depool

$FT contract --create depool --deploy FakeDePool --deployer freeton_giver || exit 2

$FT watch --account depool &> watch-depool.log &

