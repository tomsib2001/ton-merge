#!/bin/bash

. ./env.sh

FT_SWITCH=${NETWORK}
export FT_SWITCH



cmd ${FREETON_RELAY} --get-address 1:orl_swap_addr.calc

$FT account --create orl_swap --address $(cat orl_swap_addr.calc) --contract DuneUserSwap


$FT multisig -a orl --transfer 0.1 --to depool setVestingDonor '{"donor" : "%{account:address:orl_swap}" }' || exit 2


$FT inspect -h --shard-account orl_swap --subst @%{res:0:id} --output orl_swap.block

$FT watch --account orl_swap &> watch-orl.log &

cmd ${FREETON_RELAY} --set-secret 1:bonjour
