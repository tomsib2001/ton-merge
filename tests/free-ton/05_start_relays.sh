#!/bin/bash

. ./env.sh

FT_SWITCH=${NETWORK}
export FT_SWITCH



export RELAY=1
export TON_MERGE_DB="${RELAY1_DATABASE}"
export TON_MERGE_PASSPHRASE="${RELAY1_PASSPHRASE}"

echo COMMAND ${FREETON_RELAY} --daemon '&>' ${TON_MERGE_DB}.log 
cmd ${FREETON_RELAY} --daemon &> ${TON_MERGE_DB}.log  &



export RELAY=2
export TON_MERGE_DB="${RELAY2_DATABASE}"
export TON_MERGE_PASSPHRASE="${RELAY2_PASSPHRASE}"

echo COMMAND ${FREETON_RELAY} --daemon '&>' ${TON_MERGE_DB}.log 
cmd ${FREETON_RELAY} --daemon &> ${TON_MERGE_DB}.log  &



export RELAY=3
export TON_MERGE_DB="${RELAY3_DATABASE}"
export TON_MERGE_PASSPHRASE="${RELAY3_PASSPHRASE}"

echo COMMAND ${FREETON_RELAY} --daemon '&>' ${TON_MERGE_DB}.log 
cmd ${FREETON_RELAY} --daemon &> ${TON_MERGE_DB}.log  &
